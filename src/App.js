import React, { useEffect } from 'react';
import './App.css';
import Header from './Header';
import Sidebar from './Sidebar';
import Feed from './Feed';
import { useSelector, useDispatch } from 'react-redux';
import { selectUser } from './features/userSlice';
import { auth } from './firebase';
import Login from './Login';
import { login, logout } from './features/userSlice';
import Chat from './Chat';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import SidebarChat from './SidebarChat';
import MainbarChat from './MainbarChat';
import Messages from './Messages';


function App() {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  useEffect(() => {
    auth.onAuthStateChanged((authUser) => {
      console.log("user is", authUser);
      if (authUser) {
        dispatch(login({
          uid: authUser.id,
          displayName: authUser.displayName,
          photoURL: authUser.photoURL,
        }))
      }
      else {
        dispatch(logout());

      }
    })
  }, [dispatch])
  return (

    <div className="app">
      {!user ? (
        
        <Login />


      ) : (
        <>
          <Router>
            <Switch>
              <Route exact path="/">
                <Header />
                  <div className="app__body">
                  <Sidebar />
                  <Feed />
                  </div>
              </Route>
              <Route exact path='/chat'>
                
                
                <Chat />
                <div className="app__body">
                <Route exact path="/chat/rooms/:roomId">
                  <Chat/>
                  
              

                </Route>
               
                       
                </div>
              </Route>
               </Switch>
          </Router>
        </>

      )}
    </div>

  );
}

export default App;
