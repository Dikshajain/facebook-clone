import React from 'react'
import './Chat.css';
import MainbarChat from './MainbarChat';
import SidebarChat from './SidebarChat';

function Chat() {
    return (
        <div className="chat">

            <div className="sidebar">
                <SidebarChat/>
            </div>

            <div className="mainbar">
                <MainbarChat/>
            </div>
        </div>
    )
}

export default Chat
