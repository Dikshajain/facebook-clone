import React,{useState,useEffect} from 'react'
import './SidebarChat.css';
import Sidebarchatroom from './Sidebarchatroom';
import db from './firebase';
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';
import { Avatar } from '@material-ui/core';




function SidebarChat() {
    const user=useSelector(selectUser);
    const[rooms,setRooms]=useState([]);
    useEffect(()=>{
    db.collection("rooms").onSnapshot((snapshot=>
        setRooms(
            snapshot.docs.map((doc)=>({
                id:doc.id,
                data:doc.data(),
            }))
        )))
    },[])
   
    return (
        <div className="sidebarChat">
            <div className="sidebarchat__header">
                <Avatar src={user.photoURL}/>
                <h2>{user.displayName}</h2>
                </div>
                <div className="sidebarchatroomd">
                    <Sidebarchatroom addNewChat />
                   {rooms.map(room=>(
                       <Sidebarchatroom key={room.id} id={room.id} name={room.data.name}/>
                   ))}
            </div>
        </div>
    )
}

export default SidebarChat
