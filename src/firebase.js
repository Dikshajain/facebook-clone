// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import firebase from "firebase";


const firebaseConfig = {
    apiKey: "AIzaSyCnfW2SCvm5vgDiTrHRSMmHLVYYrseqIEQ",
    authDomain: "facebook-clone-7beb0.firebaseapp.com",
    projectId: "facebook-clone-7beb0",
    storageBucket: "facebook-clone-7beb0.appspot.com",
    messagingSenderId: "364355917306",
    appId: "1:364355917306:web:a20bcb3e72a416580dc043",
    measurementId: "G-7FQD2T5ZYE"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth=firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();

  export { auth,provider };
  export default db;