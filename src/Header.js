import React from 'react';
import "./Header.css";
import logo from "../src/images/FaceB.png";
import SearchIcon from '@material-ui/icons/Search';
import HomeIcon from '@material-ui/icons/Home';
import FlagIcon from '@material-ui/icons/Flag';
import SubscriptionsOutlinedIcon from '@material-ui/icons/Subscriptions';
import StorefrontOutlinedIcon from '@material-ui/icons/Storefront';
import SupervisedUserCircleIcon from '@material-ui/icons/SupervisedUserCircle';
import {Avatar, IconButton} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import ForumIcon from '@material-ui/icons/Forum';
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {selectUser} from './features/userSlice';
import {useSelector,useDispatch} from 'react-redux';
import {logout} from './features/userSlice';
import {auth} from './firebase';

import {Link} from 'react-router-dom';


function Header() {
   
    const user=useSelector(selectUser);
    const dispatch=useDispatch();

   const handleOut=()=>{
       dispatch(logout())
       auth.signOut();

       
   }
 
    return (
        <div className="header">
            <div className="header__left">
            
                <img src={logo} alt="facebook" />
              
                <div className="header__input">
                    <SearchIcon />
                    <input type="text" placeholder="Search Facebook"/>
                </div>

            </div>

            <div className="header__middle">
                <div className="header__option header__option-active">
                    <HomeIcon fontSize="large"/>
                </div>
                <div className="header__option">
                    <FlagIcon fontSize="large"/>
                </div>
                <div className="header__option">
                    <SubscriptionsOutlinedIcon fontSize="large"/>
                </div>
             <div className="header__option">
                    <StorefrontOutlinedIcon fontSize="large"/>
                </div>
                <div className="header__option">
                    <SupervisedUserCircleIcon fontSize="large"/>
                </div>

            </div>
            <div className="header__right">
                <div className="header__info">
                    <Avatar src={user.photoURL} onClick={handleOut}/>
                    <h4>{user.displayName}</h4>
                </div>
                <IconButton>
                    <AddIcon />
                </IconButton>


             
                <IconButton component={Link} to='/chat'>
              
                    <ForumIcon />
                   
                </IconButton>
                
                
                <IconButton>
                    <NotificationsActiveIcon />
                </IconButton>
                
                <IconButton>
                    <ExpandMoreIcon />
                </IconButton>
                


            </div>
        </div>
    )
}

export default Header; 
