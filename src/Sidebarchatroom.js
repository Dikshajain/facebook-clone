import React,{useEffect,useState} from 'react'
import './Sidebarchatroom.css';
import {Avatar} from '@material-ui/core';
import db from './firebase';
import {Link} from 'react-router-dom';

function Sidebarchatroom({id,name,addNewChat}) {
    const[messages,setMessages]=useState("");
      
    useEffect(() => {
        if(id){
            db.collection('rooms').doc(id).collection('messages').orderBy('timestamp','desc').onSnapshot(snapshot => {
                setMessages(snapshot.docs.map((doc) => doc.data()))
            })
        }
    }, [id]);

    
    const createChat=()=>{
   

        var roomName=prompt("Enter new person name");
        if(roomName){
            
            db.collection("rooms").add({
                name:roomName,                                                                      
            })
        }
    }
    return !addNewChat ? (
        <Link to={`/chat/rooms/${id}`} key={id}>
        <div className='sidebarchatroom'>
            <div className="sidebarchatroom__info">
            <Avatar />
            <h3>{name}</h3>
            </div>
            <div className="sidebarchatroom__message">
                <p>{messages[0]?.message}</p>
            </div>
        </div>
        </Link>
    ):(
        <div onClick={createChat} className="sidebarChat">
        <h3 className="add-new-chat-title">Add New Chat</h3>
    </div>
    )
}

export default Sidebarchatroom
