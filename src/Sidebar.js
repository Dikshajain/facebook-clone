import React, { useState } from 'react';
import './Sidebar.css';
import SidebarRow from './SidebarRow';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import EmojiFlagsIcon from '@material-ui/icons/EmojiFlags';
import ChatIcon from '@material-ui/icons/Chat';
import PeopleIcon from '@material-ui/icons/People';
import StorefrontIcon from '@material-ui/icons/Storefront';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import ExpandMoreOutlinedIcon from "@material-ui/icons/ExpandMoreOutlined";
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';



function Sidebar() {
    const user=useSelector(selectUser);
    
    return (
        <div className="sidebar">
            <SidebarRow src={user.photoURL} 
                    title={user.displayName}/>
            <SidebarRow icon={<LocalHospitalIcon />} 
            title="COVID-19 
            Information 
            Center"/>
            <SidebarRow  icon={<EmojiFlagsIcon />}  title="Pages"/>
            <SidebarRow  icon={<PeopleIcon />} title="Friends"/>
            <SidebarRow  icon={<ChatIcon />} title="Messenger"/>
            <SidebarRow  icon={<StorefrontIcon />} title="MarketPlace"/>
            <SidebarRow  icon={<VideoLibraryIcon />} title="Videos"/>
            <SidebarRow  icon={<ExpandMoreOutlinedIcon />} title="See More"/>
            
        </div>
    )
}


export default Sidebar;









