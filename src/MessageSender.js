import React,{useState,useEffect} from 'react';
import {Avatar} from  '@material-ui/core';
import VideocamIcon from '@material-ui/icons/Videocam';
import PhotoLibraryIcon from '@material-ui/icons/PhotoLibrary';
import InsertEmoticonIcon from '@material-ui/icons/InsertEmoticon';
import './MessageSender.css';
import db from "./firebase";
import firebase from "firebase";
import Post from './Post';
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';

function MessageSender() {
    const [input,setInput] = useState("");
    const[imageurl,setImageurl]=useState("");
    const[posts,setPosts] = useState([]);
    const user=useSelector(selectUser);

    useEffect(() => {
        db.collection("posts").orderBy("timestamp","desc").onSnapshot(snapshot=>{
            setPosts(snapshot.docs.map(doc=>(
                {
                    id:doc.id,
                    data:doc.data(),
                }
            )))
        })
     }, [])
    

const handleSubmit = (e) =>{
    e.preventDefault();

    db.collection('posts').add({

        message:input,
        timestamp:firebase.firestore.FieldValue.serverTimestamp(),
        image:imageurl,
        profilepic:user.photoURL,
        username:user.displayName, 
    })

    setInput("");
    setImageurl("");
};

const inputValue1 = (e) =>{
        setInput(e.target.value);
}
const inputValue2 = (e) => {
        setImageurl(e.target.value);
}


   
    return (
        
        <div className="message__sender">
            <div className="messagesender__top">
                <Avatar src={user.photoURL}/>
                <form>
                <input className="messageSender__input" 
                placeholder= {`What's on your mind,${user.displayName}?`}
                 value={input}
                 onChange={inputValue1} />
                <input placeholder="image URL (Optional)" 
                 value={imageurl}
                 onChange={inputValue2} />
                <button onClick={handleSubmit} type="submit">Hidden submit</button>
                </form>
               
            </div>

            <div className="messagesender__bottom">
                    <div className="messagesender__option">
                        <VideocamIcon style={{color:"red"}} />
                        <h3>Live Video</h3>
                        </div>
                        <div className="messagesender__option">
                        <PhotoLibraryIcon style={{color:"green"}}/>
                        <h3>Photo/Video</h3>
                        </div>
                        <div className="messagesender__option">
                        <InsertEmoticonIcon style={{color:"orange"}}/>
                        <h3>Feeling/Activity</h3>
                        </div>
                    
            </div>
            
            {posts.map(({id,data:{profilepic,image,username,message}})=>(
                <Post key={id}
                        name={username}
                        message={message}
                        image={image}
                        profilepic={profilepic}
                        />
            ))}

      
          

        </div>
    
    )
}

export default MessageSender;
