import React from 'react';
import {Avatar, Icon} from "@material-ui/core";

function SidebarRow({src,icon,title}) {
    return (
        <div className="sidebarrow">
            {src && <Avatar src={src} />}
            {icon}
            <h4>{title}</h4>
            
        </div>
    )
}

export default SidebarRow;
