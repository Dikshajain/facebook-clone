import React, { useState,useEffect} from 'react';
import './Post.css';
import {Avatar} from '@material-ui/core';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import ChatBubbleOutlinedIcon from '@material-ui/icons/ChatBubbleOutlineOutlined';
import NearMeIcon from '@material-ui/icons/NearMe';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import ExpandMoreOutlinedIcon from '@material-ui/icons/ExpandMoreOutlined';
import { useSelector } from 'react-redux';
import { selectUser } from './features/userSlice';



function Post({profilepic,image,username,timestamp,message}) {
    const user=useSelector(selectUser);

   
    return (
        <div className="post">
            <div className="post__top">
                <Avatar src={user.photoURL} className="profile_pic" />
                <div className="posttop__info">
                    <h4>{user.displayName}</h4>
                    <p>{new Date(timestamp?.toDate()).toUTCString()}</p>
                </div>
            </div>
            <div className="post__middle">
                <p>{message}</p>
            </div>

            <div className="post__bottom">
                <img src={image} alt="" />
            </div>

            <div className="post__options">
                <div className="post__option">
                    <ThumbUpIcon />
                    <p>Like</p>
                </div>
                <div className="post__option">
                    <ChatBubbleOutlinedIcon />
                    <p>Comment</p>
                </div>
                <div className="post__option">
                    <NearMeIcon />
                    <p>Share</p>
                </div>
                <div className="post__option">
                    <AccountCircleIcon />
                    <ExpandMoreOutlinedIcon />
                </div>
               
                   
                   
              

            </div>

            
        </div>
    );
}

export default Post;
