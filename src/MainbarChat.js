import { Avatar, Button } from '@material-ui/core'
import { AddAlarm, AddAlert } from '@material-ui/icons'
import React,{useState,useEffect} from 'react'
import './Mainbarchat.css';
import Messages from './Messages';
import {useParams} from 'react-router-dom';
import db from './firebase';

function MainbarChat() {
    const[roomName,setRoomName]=useState("");
    const{roomId}=useParams();

  useEffect(()=>{
        if(roomId){
            db.collection('rooms').doc(roomId).onSnapshot(snapshot=>{
                setRoomName(snapshot.data().name);
            })
        }
  },[roomId])


    return  (!roomId) ? (
        
         <div className="mainbarChat">
                <h4>No new messages</h4>
                </div>
        ):
        (
        <div className="mainbarChat">
          <div className="mainbarchat__header">

            
                <Avatar/>
                <h3>{roomName}</h3>
               
                
                    <AddAlarm/>
                    <AddAlert/>
                </div>
              
       

            <div className="mainbarchat__feed">
                <Messages title="" />
            </div>

            <div className="mainbarchat__input">
                <input type="text" placeholder="messages"/>
                <Button type="submit">send</Button>
            
            </div>
        </div>
    )
}

export default MainbarChat
