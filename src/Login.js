import React from 'react';
import './Login.css';
import logo from "../src/images/FaceB.png";
import fblogo from "../src/images/facebook.jpg";
import Button from '@material-ui/core/Button';
import { auth,provider } from "./firebase";





function Login() {
    
    
    const signIn = () =>{
        //sign in
        auth
        .signInWithPopup(provider)
        
        
        .catch((error) => alert(error.message));
    };
    return (
        <div className="login">
            <div className="login__logo">
                <img src={logo} alt="" />
                <img src={fblogo} alt="" className="img2" />


            </div>
            <Button type="submit" onClick={signIn}>Sign In</Button>
            
        </div>
    )
}

export default Login;

